# EP1 - OO (UnB - Gama)

----------------
EP1- OO(UnB-GAMA
----------------


O presente software, desenvolvido em c++ tem a finalidade de aplicar os seguintes filtros em imagems
do tipo .ppm:

    Filtro negativo
    Filtro polarizado
    Filtro preto e branco
    Filtro média 3x3

Para tanto possui as funcionalidades de abrir uma imagem do tipo,aplicar um
dado filtro e poder salvar as alterações para um novo arquivo.

---------------------
Estrutura do programa
---------------------

O programa está estruturado da seguinte forma:

EP1

Diretório raiz

EP1/src

    Diretório onde se encontram os arquivos de implementação(.cpp)
EP1/inc

    Diretório onde se encontram os arquivos de cabeçalho(.hpp)

EP1/bin

    Diretório responsável por  armazenar o arquivo executável.

EP1/obj

    Diretório responsável por armazenar os arquivos objetos(.o)

EP1/doc

    Diretório possuindo amostras de imagens .ppm

---------------------
Pré-requisitos de uso
---------------------

Para fazer uso do programa, é preciso ter os seguintes pré-requisitos:

-Make(Pacote responsável por organizar a compilação dos arquivos)
-g++(Compilador da linguagem c++)

------------------------
Instruções de instalação
------------------------

Estando com um terminal aberto dentro do diretório raiz de EP1, basta executar
o comando:

Make

Fazendo isso, o make cuidará da compilação e gerará um arquivo executável na
pasta bin

----------------------
Instruções de execução
----------------------

Estando com um terminal aberto dentro do diretório raiz de EP1, basta executar
o comando:

Make run

Fazendo isso, o make cuidará em executar o programa

O programa a principio requisita ao usuário uma das opções disponíveis

Abrir Imagem 

    Apos escolher esta opção basta digitar o diretório onde se encontra o arquivo ppm(Diretório raiz padrão), e a imagem estará elegível para aplicação de
filtros.

Aplicar Filtro

    Basta selecionar para aplicar o filtro desejado.

Salvar alterações

    Gera um arquivo com o nome escolhido no diretório raiz

-----
Autor
-----

Francisco Wallacy Coutinho Braz, estudante no curso de engenharia pela
Universidade de Brasília, Faculdade do Gama.

















    




 
