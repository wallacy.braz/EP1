//filtro.hpp
#ifndef FILTRO_HPP
#define FILTRO_HPP
#include <iostream>
#include <string>
#include "imagem.hpp"

using namespace std;

class Filtro
{
private:
string descript;
public:
Filtro();
void applyFilter(Imagem & imagem);
string getDescription();
};


#endif
