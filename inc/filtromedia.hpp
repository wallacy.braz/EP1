#ifndef FILTROMEDIA_HPP
#define FILTROMEDIA_HPP
#include "filtro.hpp"
#include "imagem.hpp"


using namespace std;

class Filtromedia :public Filtro{
public:
Filtromedia();
void applyFilter(Imagem & imagem);

};


#endif
