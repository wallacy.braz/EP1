#ifndef FILTROPEB_HPP
#define FILTRPEB_HPP
#include "imagem.hpp"
#include "filtro.hpp"


using namespace std;

class Filtropeb:public Filtro{
public:
Filtropeb();
void applyFilter(Imagem & imagem);
};

#endif
