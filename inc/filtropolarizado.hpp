#ifndef FILTROPOLARIZADO_HPP
#define FILTROPOLARIZADO_HPP
#include "filtro.hpp"
#include "imagem.hpp"
#include <iostream>
#include <string>


using namespace std;

class Filtropolarizado:public Filtro{
public:
Filtropolarizado();
void applyFilter(Imagem & imagem);
};

#endif
