//imagem.hpp

#ifndef IMAGEM_HPP
#define IMAGEM_HPP
#include <string>
#include <fstream>
#include <stdlib.h>
#include <iostream>


using namespace std;
typedef struct _header
    {
        string n_magico;
        int largura;
        int altura;
        int max_color;
    }header;

typedef struct _pixel
    {
        unsigned char r,g,b;
    }pixel;

class Imagem
{
    private:
    header header_obj;
    pixel ** pixels;

    public:
    Imagem();
    Imagem(string dir);
    ~Imagem();
    void setHeader(header header_obj);
    header getHeader();
    void setPixels(pixel **& pixels);
    pixel ** getPixels();
    void  abrirImagem(ifstream & myfile,string dir);
    void lerImagem(header * header_temp, pixel  **& temp,ifstream & myfile);
    void copiarObjetoImagem();


};

#endif
