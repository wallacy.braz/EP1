#include "filtronegativo.hpp"
#include <string>
#include <iostream>

using namespace std;

Filtronegativo::Filtronegativo(){
}

void Filtronegativo::applyFilter(Imagem & imagem)
{
    int altura,largura,i,j,max_color;
    pixel ** pixels;
    altura = imagem.getHeader().altura;
    largura = imagem.getHeader().largura;
    max_color = imagem.getHeader().max_color;
    pixels = (pixel**)malloc(sizeof(pixel*)*altura);
    for(i=0;i<largura;i++)
    {
        pixels[i] = (pixel*)malloc(sizeof(pixel)*largura);
    }
    for(i=0;i<altura;i++)
    {
        for(j=0;j<largura;j++)
        {
            pixels[i][j].r = imagem.getPixels()[i][j].r;
            pixels[i][j].r = max_color - pixels[i][j].r;
            pixels[i][j].g = imagem.getPixels()[i][j].g;
            pixels[i][j].g = max_color - pixels[i][j].g;
            pixels[i][j].b = imagem.getPixels()[i][j].b;
            pixels[i][j].b = max_color - pixels[i][j].b;
        }
    }
    
    imagem.setPixels(pixels);

}
