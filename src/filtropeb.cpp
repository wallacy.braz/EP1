#include "filtropeb.hpp"

using namespace std;


Filtropeb::Filtropeb(){
}

void Filtropeb::applyFilter(Imagem & imagem)
{
    int i,j,altura,largura,gray_scale;
    pixel ** pixels;
    altura = imagem.getHeader().altura;
    largura = imagem.getHeader().largura;
    pixels = (pixel**)malloc(sizeof(pixel*)*altura);    
    for(i=0;i<altura;i++)
    {
        pixels[i] = (pixel*)malloc(sizeof(pixel)*largura);
    }
    for(i=0;i<altura;i++)
    {
        for(j=0;j<largura;j++)
        {
          gray_scale = (0.299*imagem.getPixels()[i][j].r) + (0.587 *imagem.getPixels()[i][j].g) + (0.144 * imagem.getPixels()[i][j].b); 
          pixels[i][j].r = gray_scale;
          pixels[i][j].g = gray_scale;
          pixels[i][j].b = gray_scale;
        }
    }

    imagem.setPixels(pixels);

}
