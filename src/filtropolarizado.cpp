#include "filtropolarizado.hpp"

using namespace std;

Filtropolarizado::Filtropolarizado(){
}


void Filtropolarizado::applyFilter(Imagem & imagem)
{
    int largura,altura,i,j;
    int max_color;
    pixel ** pixels;
    largura = imagem.getHeader().largura;
    altura = imagem.getHeader().altura;
    max_color = imagem.getHeader().max_color;
    pixels = (pixel**)malloc(sizeof(pixel*)*altura);
    for(i=0;i<altura;i++)
    {
        pixels[i] = (pixel*) malloc(sizeof(pixel)*largura);
    }
    for(i=0;i<altura;i++)
    {
        for(j=0;j<largura;j++)
        {
            pixels[i][j].r = imagem.getPixels()[i][j].r;
            if(pixels[i][j].r < max_color/2){
            pixels[i][j].r = 0;
            }
            else{
            pixels[i][j].r = max_color;
            }
    
            pixels[i][j].g = imagem.getPixels()[i][j].g;
            if(pixels[i][j].g < max_color/2){
            pixels[i][j].g = 0;
            }
            else{
            pixels[i][j].g = max_color;
            }

            pixels[i][j].b = imagem.getPixels()[i][j].b;
            if(pixels[i][j].b < max_color/2){
            pixels[i][j].b = 0;
            }
            else{
            pixels[i][j].b = max_color;
            }
            
        }
    }
    
    imagem.setPixels(pixels);
}
