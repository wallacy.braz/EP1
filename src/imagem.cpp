//imagem.cpp
#include "imagem.hpp"
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

Imagem::Imagem(){
}

//Já abre uma imagem;
Imagem::Imagem(string dir)
{
    ifstream myfile;
    header header_temp;
    pixel ** pixels;
    this->abrirImagem(myfile,dir);
    this->lerImagem(&header_temp,pixels,myfile);
    this->setHeader(header_temp);
    this->setPixels(pixels);
    myfile.close();
}

void Imagem::setHeader(header header_obj)
{
    this->header_obj.n_magico = header_obj.n_magico;
    this->header_obj.largura = header_obj.largura;
    this->header_obj.altura = header_obj.altura;
    this->header_obj.max_color = header_obj.max_color;
}

header Imagem::getHeader()
{
    return header_obj;
}

void Imagem::setPixels(pixel **& pixels)
{

    int altura;
    int largura;
    altura = this->getHeader().altura;
    largura = this->getHeader().largura;
    int i,j;
    this->pixels = (pixel**)malloc(sizeof(pixel*)*altura);
    for(i=0;i<altura;i++)
    {
        this->pixels[i] = (pixel*)malloc(sizeof(pixel)*largura);
    }

    for(i=0;i<altura;i++)
    {
        for(j=0;j<largura;j++)
        {
           this->pixels[i][j].r =pixels[i][j].r;
           this->pixels[i][j].g =pixels[i][j].g;
           this->pixels[i][j].b =pixels[i][j].b;
        }
    }
}

pixel ** Imagem::getPixels()
{
    return pixels;
}

void Imagem::abrirImagem(ifstream& myfile,string dir)
{
myfile.open(dir.c_str());
}


//um header temporário alocado mas não inicializado
//um pixel ** , temporario mais não alocado
//um ifstream myfile
void Imagem::lerImagem(header * header_temp, pixel  **& temp,ifstream& myfile)
{
    string aux;
    int i,j,z;
    i=0;
    string largura,altura;
    unsigned char uC;
    while(i!=3)
    {
        getline(myfile,aux);
        if(aux[0]!='#')
        {
            switch(i)
            {
                case 0:
                {
                    header_temp->n_magico = aux;
                    break;
                }
                case 1:
                {

                    if(aux.size()==7)
                    {
                        largura = aux.substr(0,3);
                        altura = aux.substr(3,7);

                    }
                    else
                    {
                        largura = aux;
                        getline(myfile,aux);
                        if(aux[0]=='#')
                        {
                            getline(myfile,aux);
                            altura = aux;
                        } else {
                            altura = aux;
                        }
                    }
                    header_temp->largura = atoi(largura.c_str());
                    header_temp->altura = atoi(altura.c_str());


                }

                case 2:
                {
                    string max_color;
                    max_color = aux;
                    header_temp->max_color = atoi(max_color.c_str());
                    break;
                }

            }
            i++;
        }

    }

    temp=(pixel**)malloc(sizeof(pixel*)*header_temp->altura);
    for(j=0;j<header_temp->altura;j++)
    {
        temp[j]= (pixel*)malloc(sizeof(pixel)*header_temp->largura);
    }
    for(j=0;j<header_temp->altura;j++)
    {
       for(z=0;z<header_temp->largura;z++)
       {
           uC =myfile.get();
           temp[j][z].r = uC;
           uC =myfile.get();
           temp[j][z].g = uC;
           uC =myfile.get();
           temp[j][z].b = uC;

       }
   }



   //fim da alocação e inicialização|em temp;

}

void Imagem::copiarObjetoImagem()
{
        string dir;
        int i,j;
        unsigned char c;
        cout << "Digite o nome do arquivo" << endl;
        cin >> dir;
        ofstream  myfile(dir.c_str());
        myfile << this->header_obj.n_magico << endl;
        myfile << this->header_obj.largura << endl;
        myfile << this->header_obj.altura << endl;
        myfile << this->header_obj.max_color << endl;
        for(i=0;i<this->header_obj.altura;i++)
        {
            for(j=0;j<this->header_obj.largura;j++)
            {
                c = this->pixels[i][j].r;
                myfile << c;
                c = this->pixels[i][j].g;
                myfile << c;
                c = this->pixels[i][j].b;
                myfile << c;
            }
        }

        myfile.close();

}

