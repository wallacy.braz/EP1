#include "imagem.hpp"
#include "filtro.hpp"
#include "filtronegativo.hpp"
#include "filtropolarizado.hpp"
#include "filtropeb.hpp"
#include "filtromediatres.hpp"


using namespace std;


int main()
{
    int opcao;
    Imagem * ppm;
    ppm = NULL;
    while(1)
    {
         cout <<"1-Abrir Imagem" << endl;
         cout <<"2-Aplicar filtro" << endl;
         cout <<"3-Salvar Alterações" << endl;
         cout <<"Digite a opção desejada:" << endl;
         do
         {
            cin >> opcao;
            if(opcao <1 || opcao >3)
            cout << "Opção inválida! Digite novamente" << endl;
         }while(opcao <1 || opcao >3);
         switch(opcao)
         {
                 case 1:
                 {
                     string dir;
                     cout << "Digite o nome do arquivo a ser aberto: " << endl;
                     cin >> dir;
                     ppm = new Imagem(dir);
                     system("clear");
                     break;
                 }
                 case 2:
                 {
                     if(ppm==NULL)
                     cout << "Sem objeto Imagem no momento, por favor abra uma imagem" << endl;
                     else
                     {
                        cout << "1-Filtro negativo\n2-Filtro polarizdo\n3-Filtro preto e branco\n4-Filtro média"<< endl;
                        do
                        {
                        cin >> opcao;
                        if(opcao<1 || opcao >4)
                        cout << "Opção inválida! Digite novamente" << endl;
                        }while(opcao < 1 || opcao > 4);
                        switch(opcao)
                        {
                             case 1:
                             {
                                 Filtronegativo * neg;
                                 neg = new Filtronegativo();
                                 neg->applyFilter(*ppm);
                                 break;
                             }
                             case 2:
                             {
                                 Filtropolarizado * pol;
                                 pol = new Filtropolarizado();
                                 pol->applyFilter(*ppm);
                                 break;
                             }
                             case 3:
                             {
                                 Filtropeb * peb;
                                 peb = new Filtropeb();
                                 peb->applyFilter(*ppm);
                                 break;
                             }
                             case 4:
                             {
                                 cout <<"1-media 3x3\n2-media 5x5\n3-media 7x7" <<endl;
                                 do
                                 {
                                     cin >> opcao;
                                     if(opcao <1 || opcao > 3)
                                     cout << "Opcao inválida! Digite novamente" << endl;
                                 }while(opcao < 1 || opcao >3);
                                switch(opcao)
                                {
                                    case 1:
                                    {
                                    Filtromediatres * tres;
                                    tres = new Filtromediatres();
                                    tres->applyFilter(*ppm);
                                    break;
                                    }
                                    case 2:{
                                    cout << "Não implementado!" << endl;
                                    break;
                                    }
                                    case 3:{
                                    cout << "Não implementado!" << endl;
                                    }
                                 }
                              }
                        }       
                     }
                     break;
                     system("clear");
                 }
                 case 3:
                 {
                     if(ppm==NULL){
                     cout << "Sem objeto imagem no momento, por favor abra uma imagem" << endl;
                     break;
                     }
                     ppm->copiarObjetoImagem();
                     return 0;
                 }
               
     }
   }
}
    
    


